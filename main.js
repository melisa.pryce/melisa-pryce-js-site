jQuery(".plus-one").click(addOne);
$("#minus-one").click(minusOne);

function minusOne() {

  var value = parseInt($(".answer").text()) - 1;
  $(".answer").text(value);

}

function addOne() {
  //create value variable and plus 1 to the value of the text
  var value = parseInt($(".answer").text()) + 1;

  //find div elements and set it's text to value
  $(".answer").text(value);
}
